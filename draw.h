
#ifndef __DRAW_H__
#define __DRAW_H__

#include "font_types.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

unsigned short* img_init(void);
void cleanup(unsigned short* img);

void draw_pixel(int x, int y, unsigned short color, unsigned short * img);
void draw_char(int x, int y, font_descriptor_t* fdes, char ch, unsigned short * img);
void draw_space(int x, int y, font_descriptor_t* fdes, unsigned short *img);
void draw_word(int x, int y, font_descriptor_t* fdes, char *word, unsigned short *img);
void draw_pattern(int size, unsigned short *img);
void draw_moves(int moves, unsigned short* img, font_descriptor_t* fdes, int x, int y);
void draw_time(time_t clock_start, time_t* clock, unsigned short* img, font_descriptor_t* fdes, int x, int y);

void erase_board(unsigned short* img);
void erase_all(unsigned short *img);

void get_indexes(int size);
void draw_on_index(int index, char mychar,  font_descriptor_t* fdes, unsigned short* img, int size);
void fill_board(unsigned short* img, font_descriptor_t* fdes, int size);
void bad_swap(unsigned short* img);



#endif