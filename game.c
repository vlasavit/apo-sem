
#include <stdlib.h>
#include "game.h"

game_t g;

bool init_game(int dimensions)
{
    g.dim = dimensions;
    g.nb_tiles = dimensions * dimensions - 1;
    g.blank_pos = dimensions * dimensions - 1; //set the blank tile on the last tile
    g.game_over = false;
    g.error = false;

    g.tiles = calloc(g.nb_tiles + 1, sizeof(int));
    if (g.tiles == NULL) {
        fprintf(stderr, "ERROR: Connot alocate memory\n");
        g.error = true;
    }
    return g.error;
}

void new_game(void)
{
    do {
        reset(); //reset in initial state
        shuffle();
    } while (!is_solvable()); //shuffle until grid is solvable
    g.game_over = false;
}

void reset(void)
{
    for (int i = 0; i < g.nb_tiles; i++) {
        g.tiles[i] = (i + 1);
    }
    g.tiles[g.nb_tiles] = 0; //set blank tile
    g.blank_pos = g.nb_tiles;
}

void shuffle(void)
{
    int n = g.nb_tiles;
    for (int i = n - 1; i >= 0; --i) {
        //generate a random number [0, n-1]
        int j = rand() % (i + 1);

        //swap the last element with element at random index
        int temp = g.tiles[i];
        g.tiles[i] = g.tiles[j];
        g.tiles[j] = temp;
    }
}

/*  Only half permutations are solvable
    Whenever a tile is preceded by a tile with higher value it counts
    as an inversion. With blank tile in the solved position (last),
    the number of inversions must be even for the puzzle to be solvable
*/
bool is_solvable(void)
{
    int count_inversions = 0;
    for (int i = 0; i < g.nb_tiles; i++) {
        for (int j = 0; j < i; j++) {
            if (g.tiles[j] > g.tiles[i]) {
                count_inversions++;
            }
        }
    }
    return (count_inversions % 2 == 0);
}

bool is_solved(int* state)
{
    if (state[g.nb_tiles] != 0) { // blank tile is not in solved position (last)
        return false;
    }
    for (int i = 0; i < g.nb_tiles; i++) {
        if (state[i] != i + 1) {
            return false;
        }
    }
    return true;
}

void make_solved_mat()
{
    for (int i = 0; i < (g.nb_tiles); i++) {
        g.tiles[i] = i + 1;
    }
    g.tiles[g.nb_tiles] = 0;
}

int* get_state()
{
    return g.tiles;
}

bool game_over()
{
    g.game_over = is_solved(g.tiles);
    return g.game_over;
}

bool swap_from_outside(const char param)
{
    int idx, temp;
    switch (param) {
    case 's':
        if (g.blank_pos < g.dim) {
            return false;
        }
        idx = g.blank_pos - g.dim;
        temp = g.tiles[idx];
        g.tiles[idx] = 0;
        g.tiles[g.blank_pos] = temp;
        break;
    case 'w':
        if (g.blank_pos > (g.nb_tiles - g.dim)) {
            return false;
        }
        idx = g.blank_pos + g.dim;
        temp = g.tiles[idx];
        g.tiles[idx] = 0;
        g.tiles[g.blank_pos] = temp;
        break;
    case 'd':
        if (g.blank_pos % g.dim == 0) {
            return false;
        }
        idx = g.blank_pos - 1;
        temp = g.tiles[idx];
        g.tiles[idx] = 0;
        g.tiles[g.blank_pos] = temp;
        break;
    case 'a':
        if (g.blank_pos % g.dim == g.dim - 1) {
            return false;
        }
        idx = g.blank_pos + 1;
        temp = g.tiles[idx];
        g.tiles[idx] = 0;
        g.tiles[g.blank_pos] = temp;
        break;
    default:
        return false;
        break;
    }
    g.blank_pos = idx;

    return true;
}

int get_dim()
{
    return g.dim;
}

int get_blank_pos()
{
    return g.blank_pos;
}

void print_grid(void)
{
    for (int j = 0; j < g.dim; j++) {
        fprintf(stderr, "==============");
    }
    fprintf(stderr, "\n");
    for (int i = 0; i < g.dim; i++) {
        fprintf(stderr, "|");
        for (int j = 0; j < g.dim; j++) {
            fprintf(stderr, "%4d|", g.tiles[j + i * g.dim]);
        }
        fprintf(stderr, "\n");
        for (int j = 0; j < g.dim; j++) {
            fprintf(stderr, "---------");
        }
        fprintf(stderr, "\n");
    }
}
