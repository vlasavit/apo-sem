#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include <stdlib.h>

unsigned char* lcd_init(void)
{
    unsigned char* parlcd_mem_base;
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL) {
        exit(1);
	}
    parlcd_hx8357_init(parlcd_mem_base);
    return parlcd_mem_base;
}

void refresh_lcd(unsigned char* parlcd_mem_base, unsigned short* img)
{
    unsigned short c;
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (int i = 0; i < 320; i++) {
        for (int j = 0; j < 480; j++) {
            c = img[j + i * 480];
            parlcd_write_data(parlcd_mem_base, c);
        }
    }
}

void set_led(int type, unsigned char* mem_base, uint32_t value){
    if (type == 0){
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = value;
    } else if (type == 1){
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = value;
    } else if (type == 2) {
        *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = value;
    }
}
