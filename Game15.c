/*******************************************************************
  Authors: Truharik Vaclav, Vlasak Vit
  Name: Game 15
  Date: 12. 6. 2020
  Version: 1.0
 *******************************************************************/

#define _POSIX_C_SOURCE 200112L
#define _BSD_SOURCE
#define _DEFAULT_SOURCE

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <unistd.h> // for STDIN_FILENO

#include "draw.h"
#include "font_types.h"
#include "game.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "peripherals.h"
#include "prg_serial_nonblock.h"

void call_termios(int reset);
void refresh_board(unsigned short* img, font_descriptor_t* fdes, int size, unsigned char* parlcd_mem_base);
void game_start(unsigned short* img, font_descriptor_t* fdes, unsigned char* parlcd_mem_base);
void game_ended(int moves, unsigned short* img, font_descriptor_t* fdes, unsigned char* parlcd_mem_base, time_t finish);

int main(int argc, char* argv[])
{
    font_descriptor_t* fdes3 = &font_wTahoma_88;
    font_descriptor_t* fdes4 = &font_wTahoma_44_part0;

    /*
   * Setup memory mapping which provides access to the peripheral
   * registers region of RGB LEDs, knobs and line of yellow LEDs.
   */
    unsigned char* mem_base;
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

    /* If mapping fails exit with error code */
    if (mem_base == NULL) {
        exit(1);
    }

    uint32_t full_led = 0xffffffff;
    set_led(0, mem_base, full_led);
    uint32_t counter = 0;

    unsigned char* parlcd_mem_base = lcd_init();
    unsigned short* img = img_init();

    call_termios(0);

    game_start(img, fdes3, parlcd_mem_base);
    unsigned char r;
    int check;
    while ((check = serial_getc_timeout(0, 10000, &r)) >= 0) {
        if (check > 0 && (r == '3' || r == '4' || r == 'q')) {
            break;
        } else if (check <= 0) {
            r = 'q';
            break;
        }
    }
    if (r == 'q' || check < 0) {
        erase_all(img);
        refresh_lcd(parlcd_mem_base, img);
        free(img);
        call_termios(1);
        exit(101);
    }

    // start of the game
    erase_all(img);
    int dimenze = r - 48;
    bool ret = !init_game(dimenze);
    new_game();
    get_indexes(dimenze);

    font_descriptor_t* fdes = NULL;
    switch (dimenze) {
    case 3:
        fdes = fdes3;
        uint32_t green = 0x00ff00;
        set_led(1, mem_base, green);
        set_led(2, mem_base, green);
        break;
    case 4:
        fdes = fdes4;
        uint32_t blue = 0x0000ff;
        set_led(1, mem_base, blue);
        set_led(2, mem_base, blue);
        break;
    default:
        break;
    }

    int moves = 0;
    draw_moves(moves, img, fdes3, 20, 200);
    refresh_board(img, fdes, dimenze, parlcd_mem_base);

    time_t clock_test;
    time_t* clock = &clock_test;
    time_t clock_start = time(clock);
    fprintf(stderr, "INFO: Game started\n");

    unsigned char input;
    while (ret) {
        int r = serial_getc_timeout(0, 300, &input);
        if (r > 0) {
            switch (input) {
            case 'f':
                make_solved_mat();
                fill_board(img, fdes, get_dim());
                moves += 1;
                break;
            case 'w':
            case 's':
            case 'a':
            case 'd':
                if (swap_from_outside(input)) {
                    moves = (moves + 1);
                    counter = 0;
                    set_led(0, mem_base, full_led);
                    draw_moves(moves, img, fdes3, 20, 200);
                    fill_board(img, fdes, get_dim());
                    print_grid();
                } else { // wrog swap (is at end)
                    bad_swap(img);
                    refresh_lcd(parlcd_mem_base, img);
                    sleep(0.5);
                    fill_board(img, fdes, get_dim());
                }
                break;
            case 'q':
                ret = false;
                fprintf(stderr, "INFO: Exiting game\n");
                break;
            default:
                fprintf(stderr, "WARN: Wrong input\n");
                break;
            }
        } else if (r == 0) {
            counter <<= 1;
            counter++;
            if (counter < full_led) {
                set_led(0, mem_base, ~(counter & full_led));
            } else {
                moves = (moves + 1);
                counter = 0;
                set_led(0, mem_base, full_led);
                draw_moves(moves, img, fdes3, 20, 200);
            }
        } else {
            fprintf(stderr, "ERROR: Wrong reading of input\n");
            ret = false;
        }

        draw_time(clock_start, clock, img, fdes4, 28, 50);
        refresh_lcd(parlcd_mem_base, img);
        if (moves >= 999 || (time(clock) - clock_start) >= 600) {
            fprintf(stderr, "INFO: It took you too long\n");
            ret = false;
        }

        if (ret) {
            ret = !game_over();
            if (!ret) {
                fprintf(stderr, "INFO: Game finished successfully\n");
            }
        }
    }
    double finish_time = ((double)(time(clock) - clock_start));
    uint32_t led_off = 0;
    if (game_over()) {
        for (int i = 0; i < 6; i++) {
            if (i % 2 == 0) {
                set_led(0, mem_base, full_led);
            } else {
                set_led(0, mem_base, led_off);
            }
            sleep(1);
        }
    } else {
        set_led(0, mem_base, led_off);
    }

    set_led(1, mem_base, led_off);
    set_led(2, mem_base, led_off);
    game_ended(moves, img, fdes3, parlcd_mem_base, finish_time);

    call_termios(1);
    cleanup(img);

    return 0;
}

// - function -----------------------------------------------------------------
void refresh_board(unsigned short* img, font_descriptor_t* fdes, int size, unsigned char* parlcd_mem_base)
{
    fill_board(img, fdes, size);
    refresh_lcd(parlcd_mem_base, img);
}

// - function -----------------------------------------------------------------
void game_start(unsigned short* img, font_descriptor_t* fdes, unsigned char* parlcd_mem_base)
{
    //Welcome screen and settings
    draw_word(200, 120, fdes, "15", img);
    printf("15\n");
    refresh_lcd(parlcd_mem_base, img);
    erase_all(img);
    sleep(2);
    draw_word(93, 120, fdes, "Welcome", img);
    printf("Welcome\n");
    refresh_lcd(parlcd_mem_base, img);
    erase_all(img);
    sleep(2);

    draw_word(83, 20, fdes, "Choose diff", img);
    printf("Choose dificulty\n");
    draw_word(155, 120, fdes, "Press", img);
    printf("Press\n");
    draw_word(151, 220, fdes, "3 or 4", img);
    printf("3 or 4\n");
    refresh_lcd(parlcd_mem_base, img);
}

// - function -----------------------------------------------------------------
void game_ended(int moves, unsigned short* img, font_descriptor_t* fdes, unsigned char* parlcd_mem_base, time_t finish)
{
    erase_all(img);
    draw_word(69, 20, fdes, "Game over", img);
    fprintf(stderr, "Game over\n");
    if (game_over()) {
        draw_word(114, 120, fdes, "You win ", img);
        refresh_lcd(parlcd_mem_base, img);
        sleep(2);
        erase_all(img);

        int min = ((int)(finish)) / 60;
        int sec = ((int)(finish)) % 60;
        char buff[20];
        if (sec < 10) {
            sprintf(buff, "%d:0%d", min, sec);
        } else {
            sprintf(buff, "%d:%d", min, sec);
        }
        draw_word(73, 70, fdes, buff, img);
        draw_word(250, 70, fdes, "time", img);
        fprintf(stderr, "You win\ntime: %d:%d \n", min, sec);

        draw_moves(moves, img, fdes, 68, 170);
        draw_word(204, 170, fdes, "moves", img);
        fprintf(stderr, "%d moves\n", moves);
    } else {
        draw_word(106, 120, fdes, "You lose", img);
        fprintf(stderr, "You lose\n");
    }
    refresh_lcd(parlcd_mem_base, img);
}

// - function -----------------------------------------------------------------
void call_termios(int reset)
{
    static struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tio);
    if (reset) {
        tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    } else {
        tioOld = tio; // backup
        cfmakeraw(&tio);
        tio.c_oflag |= OPOST;
        tcsetattr(STDIN_FILENO, TCSANOW, &tio);
    }
}
