
#ifndef __GAME_H__
#define __GAME_H__


#include <stdio.h>
#include <stdbool.h>

typedef struct Game {
    int dim; //width and lenght of grid
    int nb_tiles;    //number of tiles
    int *tiles;     //pointer to grid of tiles
    int blank_pos;   //position of the blank tile
    bool game_over; //true if game over, false otherwise

    bool error;     //true if something goes wrong
} game_t;

bool init_game (int dimensions);
void new_game(void);

void reset(void);
void shuffle(void);
bool is_solvable(void);
bool is_solved(int * state);
void make_solved_mat();

int *get_state();
bool game_over();
bool swap_from_outside(const char param);
int get_dim();
int get_blank_pos();
void print_grid(void);

#endif