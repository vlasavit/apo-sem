
#include "draw.h"
#include "font_types.h"
#include "game.h"
#include "peripherals.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef FONT_COLOR
#define FONT_COLOR 0xffff
#endif

#ifndef BACK_COLOR
#define BACK_COLOR 0x0000
#endif

#define WIDTH 480
#define HEIGHT 320

#define SPACE_WIDTH 16

struct index_table {
    int size2;
    int* x;
    int* y;
    int step;
} index_table;

unsigned short* img_init(void)
{
    unsigned short* img = (unsigned short*)calloc(WIDTH * HEIGHT, sizeof(unsigned short));
    if (img == NULL) {
        fprintf(stderr, "ERROR: Memory allocation failed\n");
        return NULL;
    }
    for (int i = 0; i < HEIGHT; i++) {
        for (int j = 0; j < WIDTH; j++) {
            img[j + i * WIDTH] = BACK_COLOR;
        }
    }
    return img;
}

void draw_word(int x, int y, font_descriptor_t* fdes, char* word, unsigned short* img)
{
    int i = 0;
    char ch = word[i++];
    while (ch != '\0' && x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT) {
        if (ch == ' ') {
            draw_space(x, y, fdes, img);
            if (x + SPACE_WIDTH < WIDTH) {
                x += SPACE_WIDTH;
            } else {
                break;
            }
        } else {
            int width = (fdes->width == 0) ? fdes->maxwidth : fdes->width[ch - fdes->firstchar];
            draw_char(x, y, fdes, ch, img);
            if (x + width < WIDTH) {
                x += width;
            } else {
                break;
            }
        }
        ch = word[i++];
    }
}

void draw_space(int x, int y, font_descriptor_t* fdes, unsigned short* img)
{
    int w = SPACE_WIDTH;
    for (int yi = 0; yi < fdes->height; yi++) {
        for (int xi = 0; xi < w; xi++) {
            draw_pixel(x + xi, y + yi, BACK_COLOR, img);
        }
    }
}

void draw_char(int x, int y, font_descriptor_t* fdes, char ch, unsigned short* img)
{
    if (ch < fdes->firstchar || ch > (fdes->firstchar + fdes->size)) {
        return;
    }
    font_bits_t* fdata;
    if (fdes->offset == 0) {
        fdata = fdes->bits + (ch - fdes->firstchar) * fdes->height;
    } else {
        fdata = fdes->bits + fdes->offset[(ch - fdes->firstchar)];
    }
    int w = (fdes->width == 0) ? fdes->maxwidth : fdes->width[ch - fdes->firstchar];
    for (int yi = 0; yi < fdes->height; ++yi) {
        uint16_t m = 0x8000;
        uint16_t d = *fdata++; 
        for (int xi = 0; xi < w; ++xi) {
            if (d & m) {
                draw_pixel(x + xi, y + yi, FONT_COLOR, img); 
            } else {
                draw_pixel(x + xi, y + yi, BACK_COLOR, img); 
            }
            m >>= 1;
            if (xi == 15 || xi == 31 || xi == 47 || xi == 63) {
                m = 0x8000;
                d = *fdata++;
            }
        }
    }
}

void draw_pixel(int x, int y, unsigned short color, unsigned short* img)
{
    if (x >= 0 && x < 480 && y >= 0 && y < 320) {
        img[x + 480 * y] = color;
    }
}

void draw_pattern(int size, unsigned short* img)
{
    if (size != get_dim()) {
        size = get_dim();
    }

    int width = (2 * (WIDTH / 3)) / size;
    for (int y = 0; y < HEIGHT; y++) {
        for (int x = (WIDTH / 3); x < WIDTH; x += width) {
            img[x + y * WIDTH] = FONT_COLOR;
        }
    }

    for (int y = 0; y < HEIGHT; y += width) {
        for (int x = (WIDTH / 3); x < WIDTH; ++x) {
            img[x + y * WIDTH] = FONT_COLOR;
        }
    }

    for (int y = 0; y < HEIGHT; y++) {
        img[479 + y * WIDTH] = FONT_COLOR;
    }
    for (int x = 160; x < WIDTH; x++) {
        img[x + 319 * WIDTH] = FONT_COLOR;
    }
}

void get_indexes(int size)
{
    index_table.size2 = size * size;
    index_table.x = (int*)calloc(index_table.size2, sizeof(int));
    index_table.y = (int*)calloc(index_table.size2, sizeof(int));
    index_table.step = 320 / size;

    int i = 0;
    while (i < index_table.size2) {
        for (int y = 0; y < 310; y += index_table.step) {
            for (int x = 160; x < 470; x += index_table.step) {
                index_table.x[i] = x;
                index_table.y[i] = y;
                ++i;
            }
        }
    }
}

void draw_on_index(int index, char mychar, font_descriptor_t* fdes, unsigned short* img, int size)
{
    int shift_x = 0;
    int shift_y = 0;
    if (size == 3) {
        shift_x = 35;
        shift_y = 20;
    } else if (size == 4) {
        shift_x = 30;
        shift_y = 15;
    }
    draw_char(index_table.x[index] + shift_x, index_table.y[index] + shift_y, fdes, mychar, img);
}

void cleanup(unsigned short* img)
{
    free(img);
    free(index_table.x);
    free(index_table.y);
}

void erase_board(unsigned short* img)
{
    for (int y = 0; y < HEIGHT; ++y) {
        for (int x = 160; x < WIDTH; ++x) {
            img[x + y * WIDTH] = BACK_COLOR;
        }
    }
}

void erase_all(unsigned short* img)
{
    for (int y = 0; y < HEIGHT; ++y) {
        for (int x = 0; x < WIDTH; ++x) {
            img[x + y * WIDTH] = BACK_COLOR;
        }
    }
}

void fill_board(unsigned short* img, font_descriptor_t* fdes, int size)
{
    erase_board(img);
    draw_pattern(size, img);
    int* board = get_state();
    for (int i = 0; i < index_table.size2; ++i) {
        if (board[i] > 0 && board[i] < 10) {
            draw_on_index(i, board[i] + 48, fdes, img, size);
        } else if (board[i] >= 10 && board[i] < 25) {
            draw_on_index(i, board[i] + 55, fdes, img, size);
        }
    }
}

// red blink of blank position to indicate wrong swap
void bad_swap(unsigned short* img)
{
    int index = get_blank_pos();
    int sx = index_table.x[index];
    int sy = index_table.y[index];

    for (int x = 0; x < index_table.step; x++) {
        for (int y = 0; y < index_table.step; y++) {
            draw_pixel(sx + x, sy + y, 0xf800, img);
        }
    }
}

// - function -----------------------------------------------------------------
void draw_moves(int moves, unsigned short* img, font_descriptor_t* fdes, int x, int y)
{
    char buff[20];
    if (moves < 10) {
        sprintf(buff, "00%d", moves);
    } else if (moves < 100) {
        sprintf(buff, "0%d", moves);
    } else {
        sprintf(buff, "%d", moves);
    }
    draw_word(x, y, fdes, buff, img);
}

// - function -----------------------------------------------------------------
void draw_time(time_t clock_start, time_t* clock, unsigned short* img, font_descriptor_t* fdes, int x, int y)
{
    time_t clock_set = time(clock);
    double runtime = ((double)(clock_set - clock_start));

    int min = ((int)(runtime)) / 60;
    int sec = ((int)(runtime)) % 60;
    char buff[20];
    if (sec < 10) {
        sprintf(buff, "%d:0%d", min, sec);
    } else {
        sprintf(buff, "%d:%d", min, sec);
    }
    draw_word(x, y, fdes, buff, img);
}